package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CountTest {
	@Test
	public void testDivide1 () throws Exception {
	int k = new Count().divide(4,2);
	assertEquals("Divide1 ",2,k);		
	}
	@Test
	public void testDivideZero () throws Exception {
	int k = new Count().divide(4,0);
	assertEquals("Divide by zero",Integer.MAX_VALUE,k);
	}
}

