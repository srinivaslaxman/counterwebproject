package com.qaagility.controller;

public class Count {

    public int divide(int arg1, int arg2) {
        if (arg2 == 0) {
            return Integer.MAX_VALUE;
	}
        else {
            return arg1 /arg2;
	}
    }

}
